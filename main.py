# Example PyGame showing a circle moving on screen via the keyboard
import pygame

import sys
# keep common things needed by the classes in a separate Settings class
from gui_settings import GuiSettings


def get_python_version() -> str:
    return f'{sys.version_info.major}.{sys.version_info.minor}.{sys.version_info.micro}'


def get_pygame_version() -> str:
    return f'{pygame.version.vernum}'


def main():

    settings = GuiSettings()

    # pygame setup
    pygame.init()
    pygame.display.set_caption(f'PyGame Example using PyGame {get_pygame_version()} and Python {get_python_version()}')
    # screen = pygame.display.set_mode((1280, 720))
    screen = pygame.display.set_mode((settings.screen_width, settings.screen_height))
    clock = pygame.time.Clock()
    running = True
    dt = 0
    
    player_pos = pygame.Vector2(screen.get_width() / 2, screen.get_height() / 2)

    while running:
        # poll for events
        # pygame.QUIT event means the user clicked X to close your window
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                continue

        # fill the screen with a color to wipe away anything from last frame
        screen.fill("purple")
        pygame.draw.circle(screen, settings.circle_color, player_pos, settings.circle_radius)

        keys = pygame.key.get_pressed()
        
        if keys[pygame.K_w]:
            player_pos.y -= settings.circle_move_delta * dt
        elif keys[pygame.K_s]:
            player_pos.y += settings.circle_move_delta * dt
        elif keys[pygame.K_a]:
            player_pos.x -= settings.circle_move_delta * dt
        elif keys[pygame.K_d]:
            player_pos.x += settings.circle_move_delta * dt
        else:
            pass  # ignore all other keys that were pressed

        # flip() the display to put your work on screen
        pygame.display.flip()

        # limits FPS to 60
        # dt is delta time in seconds since last frame, used for framerate-
        # independent physics.
        dt = clock.tick(60) / 1000

    pygame.quit()


if __name__ == '__main__':
    print(f'Python version: {get_python_version()}')
    print(f'PyGame version: {get_pygame_version()}')
    main()
